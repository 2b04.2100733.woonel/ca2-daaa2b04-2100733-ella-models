FROM tensorflow/serving:latest

WORKDIR /models

COPY . . 

EXPOSE 8501
RUN chown -R root:root /models/
ENV MODEL_NAME=img_classifier_1


CMD ["tensorflow_model_server", "--model_name=img_classifier_1", "--model_base_path=/models/models/img_classifier_1", "--rest_api_port=8501", "--model_name=img_classifier_2", "--model_base_path=/models/models/img_classifier_2"]